<?php declare(strict_types=1);

namespace Fittinq\Symfony\Connector\EventSubscriber;

use Fittinq\Symfony\Connector\Logging\CommandLogger;
use Fittinq\Symfony\Connector\Logging\LoggingAwareInterface;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleErrorEvent;
use Symfony\Component\Console\Event\ConsoleEvent;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Throwable;

class CommandEventSubscriber implements EventSubscriberInterface
{
    private CommandLogger $commandLogger;
    private ?Throwable $exception = null;

    public function __construct(CommandLogger $commandLogger)
    {
        $this->commandLogger = $commandLogger;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ConsoleEvents::COMMAND => 'onCommand',
            ConsoleEvents::ERROR => 'onError',
            ConsoleEvents::TERMINATE => 'onTerminate'
        ];
    }

    public function onCommand(ConsoleEvent $event): void
    {
        try {
            if (! $event->getCommand() instanceof LoggingAwareInterface) {
                return;
            }

            $this->commandLogger->logCall($event->getInput());
        } catch (Throwable) {
            // if logs fail, we should not let the service itself fail.
        }
    }

    public function onError(ConsoleErrorEvent $event): void
    {
        $this->exception = $event->getError();
    }

    public function onTerminate(ConsoleTerminateEvent $event): void
    {
        try {
            if (! $event->getCommand() instanceof LoggingAwareInterface) {
                return;
            }

            $this->commandLogger->logTermination(
                $event->getInput(),
                $event->getExitCode(),
                $this->exception
            );
        } catch (Throwable) {
            // if logs fail, we should not let the service itself fail.
        }

    }
}