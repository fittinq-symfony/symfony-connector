<?php declare(strict_types=1);

namespace Fittinq\Symfony\Connector\Logging;

use ArrayIterator;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Throwable;

class HttpClientLogger extends HttpLogger
{
    private string $requestId;

    public function logRequest(string $method, string $url, array $options): void
    {
        try {
            $this->requestId = uniqid();
            $this->logger->info(
                $this->messageFormatter->createRequestMessage(
                    $method,
                    $url,
                    new ArrayIterator($options['headers'] ?? []),
                    isset($options['json']) ? json_encode($options['json']) : ''
                ),
                ["requestId" => $this->requestId]
            );
        } catch (Throwable) {
            // if logs fail, we should not let the service itself fail.
        }
    }

    public function logServiceAvailableResponse(string $method, string $url, ResponseInterface $response): void
    {
        try {
            $statusCode = $response->getStatusCode();
            $message = $this->messageFormatter->createResponseMessage(
                $method,
                $url,
                $statusCode,
                new ArrayIterator($response->getHeaders(false)),
                $response->getContent(false)
            );

            $this->logResponseWithSeverity($message, $statusCode, ["requestId" => $this->requestId]);
        } catch (Throwable) {
            // if logs fail, we should not let the service itself fail.
        }
    }

    public function logServiceUnavailableResponse(string $method, string $url, Throwable $e): void
    {
        try {
            $message = $this->messageFormatter->createResponseServiceUnavailableMessage($method, $url, $e);
            $this->logger->critical($message, ["requestId" => $this->requestId]);
        } catch (Throwable) {
            // if logs fail, we should not let the service itself fail.
        }
    }
}
