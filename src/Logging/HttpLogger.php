<?php declare(strict_types=1);

namespace Fittinq\Symfony\Connector\Logging;

use Psr\Log\LoggerInterface;

abstract class HttpLogger
{
    protected MessageFormatter $messageFormatter;
    protected LoggerInterface $logger;

    public function __construct(LoggerInterface $logger, MessageFormatter $messageFormatter)
    {
        $this->messageFormatter = $messageFormatter;
        $this->logger = $logger;
    }

    protected function logResponseWithSeverity(string $message, int $statusCode, array $context = []): void
    {
        if ($this->httpStatusCodeIsSuccessful($statusCode)) {
            $this->logger->info($message, $context);
        } elseif ($this->httpStatusCodeIsBadRequest($statusCode)) {
            $this->logger->error($message, $context);
        } elseif ($this->httpStatusCodeIsInternalServerError($statusCode)) {
            $this->logger->critical($message, $context);
        }
    }

    private function httpStatusCodeIsSuccessful(int $statusCode): bool
    {
        return $statusCode >= 200 && $statusCode <= 299;
    }

    private function httpStatusCodeIsBadRequest(int $statusCode): bool
    {
        return $statusCode >= 400 && $statusCode <= 499;
    }

    private function httpStatusCodeIsInternalServerError(int $statusCode): bool
    {
        return $statusCode >= 500 && $statusCode <= 599;
    }
}
