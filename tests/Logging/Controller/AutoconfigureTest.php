<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Connector\Logging\Controller;

use Fittinq\Symfony\Connector\EventSubscriber\ControllerEventSubscriber;
use PHPUnit\Framework\TestCase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelEvents;

class AutoconfigureTest extends TestCase
{
    public function test_autoConfigureBeforeAndAfterControllerEvents_expectInstanceOfEventSubscriber()
    {
        $configuration = new Configuration();

        $this->assertInstanceOf(
            EventSubscriberInterface::class,
            $configuration->configure(
                new Request(),
                new Response(),
                new NotLoggingAwareController(),
            )
        );
    }

    public function test_autoConfigureBeforeAndAfterControllerEvents_expectBeforeAndAfterControllerHooks()
    {
        $events = ControllerEventSubscriber::getSubscribedEvents();
        $this->assertArrayHasKey(KernelEvents::CONTROLLER, $events);
        $this->assertArrayHasKey(KernelEvents::RESPONSE, $events);
    }
}
