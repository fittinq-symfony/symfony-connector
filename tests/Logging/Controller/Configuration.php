<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Connector\Logging\Controller;

use Fittinq\Symfony\Connector\EventSubscriber\ControllerEventSubscriber;
use Fittinq\Symfony\Connector\Logging\ControllerLogger;
use Fittinq\Symfony\Connector\Logging\MessageFormatter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Test\Fittinq\Symfony\Mock\Logger\LoggerMock;

class Configuration
{
    private ControllerEvent $beforeControllerEvent;
    private ResponseEvent $afterControllerEvent;
    private LoggerMock $logger;
    private ControllerEventSubscriber $eventSubscriber;

    public function __construct()
    {
        ControllerEventSubscriber::$LOGGED_REQUEST_SIGNATURES = [];

        $this->logger = new LoggerMock();
    }

    public function configure(
        Request $request,
        Response $response,
        IndexAwareInterface $controller,
        array $sensitiveUrls = [],
        array $sensitiveHeaders = []
    ): ControllerEventSubscriber
    {
        $this->configureBeforeControllerEvent($request, [$controller, 'index']);
        $this->configureAfterControllerEvent($request, $response);
        $this->configureControllerEventSubscriber($sensitiveUrls, $sensitiveHeaders);

        return $this->eventSubscriber;
    }

    private function configureControllerEventSubscriber(array $sensitiveUrls, array $sensitiveHeaders)
    {
        $messageFormatter = new MessageFormatter($sensitiveUrls, $sensitiveHeaders);
        $controllerLogger = new ControllerLogger($this->logger, $messageFormatter);
        $this->eventSubscriber = new ControllerEventSubscriber($controllerLogger);
    }

    private function configureBeforeControllerEvent(Request $request, callable $controller)
    {
        $this->beforeControllerEvent = new ControllerEvent(new KernelMock(), $controller, $request, 0);
    }

    private function configureAfterControllerEvent(Request $request, Response $response)
    {
        $this->afterControllerEvent = new ResponseEvent(new KernelMock(), $request, 0, $response);
    }

    public function getLogger(): LoggerMock
    {
        return $this->logger;
    }

    public function getBeforeControllerEvent(): ControllerEvent
    {
        return $this->beforeControllerEvent;
    }

    public function getAfterControllerEvent(): ResponseEvent
    {
        return $this->afterControllerEvent;
    }
}
