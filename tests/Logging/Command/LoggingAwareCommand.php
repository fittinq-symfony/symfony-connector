<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Connector\Logging\Command;

use Fittinq\Symfony\Connector\Logging\LoggingAwareInterface;
use Symfony\Component\Console\Command\Command;

class LoggingAwareCommand extends Command implements LoggingAwareInterface
{
}
