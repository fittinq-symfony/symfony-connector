<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Connector\Logging\Command;

use Fittinq\Symfony\Connector\EventSubscriber\CommandEventSubscriber;
use Fittinq\Symfony\Connector\Logging\CommandLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Event\ConsoleErrorEvent;
use Symfony\Component\Console\Event\ConsoleEvent;
use Symfony\Component\Console\Event\ConsoleTerminateEvent;
use Symfony\Component\Console\Output\NullOutput;
use Test\Fittinq\Symfony\Mock\Command\InputMock;
use Test\Fittinq\Symfony\Mock\Logger\LoggerMock;
use Throwable;

class Configuration
{
    private InputMock $input;
    private ConsoleEvent $beforeCommandEvent;
    private LoggerMock $logger;
    private CommandEventSubscriber $eventSubscriber;
    private ConsoleErrorEvent $afterCommandErrorEvent;
    private ConsoleTerminateEvent $afterCommandTerminationEvent;

    public function __construct()
    {
        $this->logger = new LoggerMock();
        $this->eventSubscriber = new CommandEventSubscriber(new CommandLogger($this->logger));
    }

    public function create(array $arguments, array $options, Command $command, int $exitCode = 0, ?Throwable $commandException = null): CommandEventSubscriber
    {
        $this->configureInput($arguments, $options);
        $this->configureBeforeCommandEvent($command);

        if ($commandException) {
            $this->configureAfterCommandErrorEvent($command, $commandException, $exitCode);
        }

        $this->configureAfterCommandTerminationEvent($command, $exitCode);
        return $this->eventSubscriber;
    }

    private function configureInput(array $arguments, array $options)
    {
        $this->input = new InputMock($arguments, $options);
    }

    private function configureBeforeCommandEvent(Command $command): void
    {
        $this->beforeCommandEvent = new ConsoleEvent($command, $this->input, new NullOutput());
    }

    private function configureAfterCommandErrorEvent(Command $command, Throwable $commandException, int $exitCode): void
    {
        $this->afterCommandErrorEvent = new ConsoleErrorEvent($this->input, new NullOutput(), $commandException, $command);
        $this->afterCommandErrorEvent->setExitCode($exitCode);
    }

    private function configureAfterCommandTerminationEvent(Command $command, int $exitCode): void
    {
        $this->afterCommandTerminationEvent = new ConsoleTerminateEvent($command, $this->input, new NullOutput(), $exitCode);
    }

    public function getLogger(): LoggerMock
    {
        return $this->logger;
    }

    public function getBeforeCommandEvent(): ConsoleEvent
    {
        return $this->beforeCommandEvent;
    }

    /**
     * @return ConsoleErrorEvent
     */
    public function getAfterCommandErrorEvent(): ConsoleErrorEvent
    {
        return $this->afterCommandErrorEvent;
    }

    /**
     * @return ConsoleTerminateEvent
     */
    public function getAfterCommandTerminationEvent(): ConsoleTerminateEvent
    {
        return $this->afterCommandTerminationEvent;
    }
}
