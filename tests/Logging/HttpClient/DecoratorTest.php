<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Connector\Logging\HttpClient;

use Fittinq\Symfony\Connector\HttpClient\LogHttpClient;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Test\Fittinq\Symfony\Mock\HttpClient\ResponseMock;
use Throwable;

class DecoratorTest extends TestCase
{
    private HttpClientInterface $decoratedHttpClientMock;
    private LogHttpClient $httpClient;

    protected function setUp(): void
    {
        parent::setUp();

        $configuration = new Configuration();
        $this->httpClient = $configuration->configure();
        $this->decoratedHttpClientMock = $configuration->getDecoratedHttpClient();
    }

    /**
     * @throws Throwable
     */
    public function test_decorateHttpClient_expectRequestToBeDelegated()
    {
        $method = 'POST';
        $url = 'https://wwww.lampenlicht.nl';
        $json = ['key' => 'value'];
        $response = new ResponseMock();
        $this->decoratedHttpClientMock->setUpResponse($response);

        $this->assertEquals($response, $this->httpClient->request($method, $url, ['json' => $json]));

        $this->decoratedHttpClientMock->expectRequestToHaveBeenMadeTo($url);
        $this->decoratedHttpClientMock->expectRequestToHaveBeenMadeWithMethod($method);
        $this->decoratedHttpClientMock->expectRequestToHaveBeenMadeWithJSON($json);
    }
}
